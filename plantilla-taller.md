# Nombre del taller

Pequeña introducción y motivación del mismo.

## Objetivos a cubrir en el taller

Descripción de los objetivos a cubrir durante el desarrollo del taller

## Público objetivo

¿A quién va dirigida? 

## Prerequisitos para los asistentes

Descripción de conocimientos mínimos necesarios, así como hardware/software que deban llevar los asistentes.

## Prerequisitos para la organización

Describir las necesidades del taller, como sala con ordenadores, software instalado, etc. La organización no garantiza que se puedan satisfacer las necesidades de los talleres propuestos.

## Tiempo

Indicar la duración del taller. Por ejemplo, desde dos horas hasta un día entero.

## Día

Indicar si se prefiere realizar el taller el primer día o el segundo.

## Comentarios

Cualquier otro comentario relevante.

## Condiciones

* [ ] Acepto seguir el [código de conducta](https://eslib.re/2019/conducta/).
* [ ] Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
* [ ] Acepto coordinarme con la organización de esLibre.


